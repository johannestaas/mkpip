'''
{name}

{desc}
'''

__title__ = '{name}'
__version__ = '0.0.1'
__all__ = ()
__author__ = '{author} <{email}>'
__license__ = '{license}'
__copyright__ = 'Copyright {year} {author}'


def main():
    pass


if __name__ == '__main__':
    main()
