{name}
{underline}

{desc}

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Simply run it::

    $ {name}

Use --help/-h to view info on the arguments::

    $ {name} --help

Release Notes
-------------

:0.0.1:
    Project created
